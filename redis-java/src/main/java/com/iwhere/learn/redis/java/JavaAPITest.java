package com.iwhere.learn.redis.java;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import redis.clients.jedis.BinaryClient.LIST_POSITION;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;

/**
 * 使用java 操作 redis
 * @author wenbronk
 * @time 2017年3月24日  下午1:05:20  2017
 */

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class JavaAPITest {

	private Jedis jedis;
	private ShardedJedis shardedJedis;
	
	/**
	 * 获取redis的基础信息
	 */
	@Test
	public void testConfiguration() {
		String ping = jedis.ping();
		System.out.println(ping);
	}
	
	
	/**
	 * 对key进行操作
	 */
	@Test
	public void testKey() {
		// key是否存在
		Boolean exists = jedis.exists("name");
		
		// 查看所有的key, 支持模糊查询
		Set<String> keys = jedis.keys("*");
		
		// del 删除键
		jedis.del("name");
		
		// key的数据类型
		String type = jedis.type("vini");
		
		System.out.println(type);
	}
	
	/**
	 * 测试String类型的
	 */
	@Test
	public void testString () {
		// set, 会发生覆盖
		String ok = jedis.set("name", "vini");	//OK
		String set = jedis.getSet("name", "被修改的值");
		// 获取value值并截取
		String getrange = jedis.getrange("name", 0, 5);
		
		// append, 直接在后面添加
		Long append = jedis.append("name", "wenbronk");
		
		
		// get
		String value = jedis.get("age");
		
		// 获取字符串的长度
		Long strlen = jedis.strlen("vini");
		
		// mset , 一次设置/获取 多个键值对
		jedis.mset("name", "vini", "age", "23");
		List<String> mget = jedis.mget("vini", "wenbronk");
		
		// 自增
		Long incr = jedis.incr("age");
		
		
		System.out.println(incr + ": " + value);
		
	}
	
	/**
	 * 操作list类型的数据
	 * 使用双向链表实现
	 * 可用于商品评论列表
	 */
	@Test
	public void testList() {
		// 左插入, 可同时插入多个值
		jedis.lpush("user", "vini", "123");
		jedis.lpush("user", "wenbronk", "456", "789");
		
		// 右插入, 可同时插入多个值
		jedis.rpush("nanan", "wenbronk", "vini");
		jedis.rpush("nanan", "who");
		
		// 在某个值之前插入
		jedis.linsert("user", LIST_POSITION.AFTER, "wenbronk", "插入新值");
		
		// 长度
		jedis.llen("user");
		
		// 角标操作
		// 获取某个角标的值
		String lindex = jedis.lindex("user", 1);
		// 修改角标的值
		jedis.lset("user", 0, "我变成第一个");
		// 获取list中某段index, 最左为0, 最右为 -1, 子串
		List<String> lrange = jedis.lrange("user", 0, 3);
		// 删除角标的值
		jedis.lrem("user", 0, "这是啥");
		// 删除区间之外的值
		jedis.ltrim("user", 2, 5);
		
		
		// 出栈
		// 两端弹出元素
		jedis.lpop("user");
		jedis.rpop("user");
		
		// 将一个列表转移到另一个列表
		jedis.rpoplpush("user", "nanan");
		
		System.out.println(lrange);
	}
	
	/**
	 * 测试set集合
	 * 运用是因为集合运算
	 */
	@Test
	public void testSet() {
		//添加
		jedis.sadd("testSet", "1", "2", "3");
		// 删除匹配的记录
		jedis.srem("testSet", "5", "6", "7");
		// 删除key
		jedis.del("testSet");
		
		// 是否存在
		jedis.sismember("testSet", "4");
		
		// 获取
		Set<String> smembers = jedis.smembers("testSet");
		// 获取元素个数
		Long scard = jedis.scard("user");
		// 随机取一个
		String srandmember = jedis.srandmember("testSet");
		String spop = jedis.spop("testSet");	// 弹出
		
		
		// 集合运算
		// 差集, 包左不包右
		jedis.sdiff("testSet", "testSet2");
		// 并集, 数据分析
		jedis.sunion("testSet", "testSet2");
		// 交集
		jedis.sinter("testSet", "testSet2");
	}
	
	
	/**
	 * 操作 hash 类型的数据
	 * 类似map, 解决存储对象的问题, 但太浪费内存了, 使用hash删列实现
	 * 不过现在都用json了...
	 * 可用在缓存商品信息
	 */
	@Test
	public void testHash() {
		// 准备数据
		Map<String,String> map = new HashMap<>();
		map.put("name","vini");
		map.put("age","24");
		map.put("birth","1992-01-26");
		
		// 赋值操作
		jedis.hmset("student", map);
		// 更新, 字段存在覆盖
		jedis.hset("student", "score", "99");
		// 字段存在不做任何操作
		jedis.hsetnx("student", "age", "25");
		// 多个字段, 字段封装在map中, 字段存在会覆盖
		jedis.hmset("student", new HashMap<>());
		
		// 取值, 根据key和字段
		String name = jedis.hget("student", "name");
		// 获取多个字段
		List<String> hmget = jedis.hmget("student", "name", "age", "birth");
		// 获取所有字段
		Map<String, String> hgetAll = jedis.hgetAll("student");
		
		// 删除, 返回被删除字段的值
//		Long hdel = jedis.hdel("student", "score", "age");
		
		// 自增, 指定字段, step
		jedis.hincrBy("student", "age", 1);
		
		// 判断字段是否存在
		jedis.hexists("student", "name");
		// 只获取字段名
		Set<String> hkeys = jedis.hkeys("student");
		// 只获取字段值
		List<String> hvals = jedis.hvals("student");
	}
	
	/**
	 * 有序集合, 有序却不重复, 使用hash散列表实现
	 * 内置排序, 但用的并不多
	 * 可用在商品销售排行榜
	 */
	@Test
	public void testZSet() {
		// 新增, 中间为分数
		jedis.zadd("zset", 10, "234");
		
		// 获取排名, 从小到大
		jedis.zrange("zset", 0, -1);
		// 从大到小获取
		jedis.zrevrange("zset", 0, -1);
		
		// 获取分数
		Double zscore = jedis.zscore("zset", "234");
		// 获取指定分数范围的元素
		jedis.zrangeByScore("zset", 80, 100);
		// 获取指定范围的个数
		jedis.zcount("zset", 80, 100);
		
		// 获取元素数量
		Long zcard = jedis.zcard("zset");
		
		// 删除元素
		jedis.zrem("zset", "234");
		// 按照排名删除
		jedis.zremrangeByRank("zset", 0, 20);
		// 按照分数范围删除
		jedis.zremrangeByScore("zset", 80, 100);
		
	}
	
	
	
	
	
	
	
	
	
	
	@Before
	public void testBefore() {
		jedis = RedisClientUtil.getRedisClient().getJedis();
		shardedJedis = RedisClientUtil.getRedisClient().getShardedJedis();
	}
	
}
