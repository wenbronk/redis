package com.iwhere.learn.redis.java;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

/**
 * 用于获取redis连接池
 * 
 * @author wenbronk
 * @time 2017年3月24日 下午1:43:36 2017
 */

@Component
public class RedisClientUtil {

	/** 非切片连接池 */
	private JedisPool jedisPool;
	/** 切片连接池 */
	private ShardedJedisPool shardedJedisPool;

	private static RedisClientUtil redisClient;

	private RedisClientUtil() {
		initPool();
		initShardedPool();
	}

	public static RedisClientUtil getRedisClient() {
		if (redisClient == null) {
			synchronized (RedisClientUtil.class) {
				if (redisClient == null)
					redisClient = new RedisClientUtil();
			}
		}
		return redisClient;
	}

	/**
	 * 初始化非切片连接池
	 */
	public void initPool() {
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(20);
		config.setMaxIdle(5);
		config.setMaxWaitMillis(10000);
		config.setTestOnBorrow(false);

		jedisPool = new JedisPool(config, "192.168.50.37", 6379);
	}

	/**
	 * 初始化切片连接池
	 */
	public void initShardedPool() {
		// 池基本配置
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(20);
		config.setMaxIdle(5);
		config.setMaxWaitMillis(1000l);
		config.setTestOnBorrow(false);

		ArrayList<JedisShardInfo> list = new ArrayList<JedisShardInfo>();
		list.add(new JedisShardInfo("192.168.50.37", 6379, "master"));

		shardedJedisPool = new ShardedJedisPool(config, list);
	}

	@Bean
	public Jedis getJedis() {
		return jedisPool.getResource();
	}

	@Bean
	public ShardedJedis getShardedJedis() {
		return shardedJedisPool.getResource();
	}

}
